package com.mba.spring.boot.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ResultEntity<T> implements Serializable {

    private static final long serialVersionUID = -5399386939021496317L;
    private String code;
    private boolean success;
    private String message;
    private String errorMessage;
    private T entity;
    private List<T> list;

    public ResultEntity() {
    }

    public ResultEntity(String code, String message, T entity) {
        this.code = code;
        this.message = message;
        this.entity = entity;
    }

    public ResultEntity(boolean success) {
        this.success = success;
        if (success) {
            this.setCode("2000");
            this.setMessage("操作成功");
        } else {
            this.setErrorMessage("操作失败");
        }
    }

    public ResultEntity(boolean success, T entity) {
        this.success = success;
        this.entity = entity;
        if (success) {
            this.setMessage("操作成功");
        } else {
            this.setErrorMessage("操作失败");
        }
    }

    public ResultEntity(String code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public ResultEntity(String code, String message, List<T> list) {
        this.code = code;
        this.message = message;
        this.list = list;
    }

    public ResultEntity(String code, boolean success, String message, String errorMessage, T entity, List<T> list) {
        this.code = code;
        this.success = success;
        this.message = message;
        this.errorMessage = errorMessage;
        this.entity = entity;
        this.list = list;
    }

    public ResultEntity setCode(String code) {
        this.code = code;
        return this;
    }

    public ResultEntity setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public ResultEntity setMessage(String message) {
        this.message = message;
        return this;
    }

    public ResultEntity setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public ResultEntity setEntity(T entity) {
        this.entity = entity;
        return this;
    }

    public ResultEntity setList(List<T> list) {
        this.list = list;
        return this;
    }
}
