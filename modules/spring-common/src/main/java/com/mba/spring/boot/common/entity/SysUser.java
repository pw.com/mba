package com.mba.spring.boot.common.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysUser implements Serializable {
    private static final long serialVersionUID = -7777226188674026667L;

    private Long id;
    private String name;
    private Integer age;
    private String email;

}
