package com.mba.spring.boot.datajpa.service;

import com.mba.spring.boot.datajpa.entity.Customer;

import java.util.List;

public interface ICustomerService {

    Customer insert(Customer bean);

    void delete(Long id);

    Customer update(Customer bean);

    Customer get(Long id);

    List<Customer> findList(Customer bean);
}
