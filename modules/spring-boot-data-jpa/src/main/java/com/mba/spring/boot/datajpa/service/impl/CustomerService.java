package com.mba.spring.boot.datajpa.service.impl;

import com.mba.spring.boot.datajpa.entity.Customer;
import com.mba.spring.boot.datajpa.repository.CustomerRepository;
import com.mba.spring.boot.datajpa.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements ICustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer insert(Customer bean) {
        return customerRepository.save(bean);
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Customer update(Customer bean) {
        return customerRepository.save(bean);
    }

    @Override
    public Customer get(Long id) {
        return customerRepository.getOne(id);
    }

    @Override
    public List<Customer> findList(Customer bean) {
        return customerRepository.findAll();
    }
}
