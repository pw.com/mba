package com.mba.spring.boot.datajpa.controller;

import com.mba.spring.boot.common.ResultEntity;
import com.mba.spring.boot.common.entity.SysUser;
import com.mba.spring.boot.datajpa.entity.Customer;
import com.mba.spring.boot.datajpa.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerService; 


    @PutMapping("/insert")
    public ResultEntity insert(@RequestBody Customer bean) {
        try{
            return new ResultEntity(true).setEntity(customerService.insert(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @DeleteMapping("/delete/{id}")
    public ResultEntity delete(@PathVariable("id") Long id) {
        try{
            customerService.delete(id);
            return new ResultEntity(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @PostMapping("/update")
    public ResultEntity update(@RequestBody Customer bean) {
        try{
            return new ResultEntity(true).setEntity(customerService.update(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @GetMapping("/get/{id}")
    public ResultEntity get(@PathVariable("id")Long id) {
        try{
            return new ResultEntity(true).setEntity(customerService.get(id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @GetMapping("/find-list")
    public ResultEntity findList(Customer bean) {
        try{
            return new ResultEntity(true).setList(customerService.findList(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }



    @GetMapping("/get-user/{id}")
    public ResultEntity getSysUser(@PathVariable("id")Long id) {
        try{
            SysUser user = new SysUser();
            user.setId(10086L);
            user.setName("中国移动");
            user.setAge(86);
            user.setEmail("10086@qq.com");
            return new ResultEntity(true).setEntity(user);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }
}
