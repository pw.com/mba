package com.mba.spring.boot.datajpa.repository;


import com.mba.spring.boot.datajpa.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
