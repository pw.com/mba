package com.mba.spring.boot.mybatis.service;

import com.mba.spring.boot.mybatis.entity.SysUser;

import java.util.List;

public interface ISysUserService {

    SysUser insert(SysUser bean);
    void delete(Long id);
    SysUser update(SysUser bean);

    SysUser get(Long id);
    List<SysUser> findList(SysUser bean);

}
