package com.mba.spring.boot.mybatis.controller;

import com.mba.spring.boot.common.ResultEntity;
import com.mba.spring.boot.mybatis.entity.SysUser;
import com.mba.spring.boot.mybatis.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sys-user")
public class SysUserController {

    @Autowired
    private ISysUserService userService;


    @PutMapping("/insert")
    public ResultEntity insert(@RequestBody SysUser bean) {
        try{
            return new ResultEntity(true).setEntity(userService.insert(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @DeleteMapping("/delete/{id}")
    public ResultEntity delete(@PathVariable("id") Long id) {
        try{
            userService.delete(id);
            return new ResultEntity(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @PostMapping("/update")
    public ResultEntity update(@RequestBody SysUser bean) {
        try{
            return new ResultEntity(true).setEntity(userService.update(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @GetMapping("/get/{id}")
    public ResultEntity get(@PathVariable("id")Long id) {
        try{
            return new ResultEntity(true).setEntity(userService.get(id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }

    @GetMapping("/find-list")
    public ResultEntity findList(SysUser bean) {
        try{
            return new ResultEntity(true).setList(userService.findList(bean));
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultEntity(false);
    }


}
