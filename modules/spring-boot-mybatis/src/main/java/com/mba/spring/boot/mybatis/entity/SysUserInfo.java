package com.mba.spring.boot.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mba.spring.boot.common.entity.SysUser;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "sys_user_info")
public class SysUserInfo extends SysUser implements Serializable {
    private static final long serialVersionUID = -7777226188674026667L;
//
//    private Long id;
//    private String name;
//    private Integer age;
//    private String email;

}
