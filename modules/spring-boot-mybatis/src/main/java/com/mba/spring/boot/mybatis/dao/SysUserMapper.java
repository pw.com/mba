package com.mba.spring.boot.mybatis.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mba.spring.boot.mybatis.entity.SysUser;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
}
