package com.mba.spring.boot.mybatis.common;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

public class Constants {

    public static final Snowflake SNOW_FLAKE = IdUtil.getSnowflake(1, 1);


}
