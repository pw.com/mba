package com.mba.spring.boot.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mba.spring.boot.mybatis.common.Constants;
import com.mba.spring.boot.mybatis.dao.SysUserMapper;
import com.mba.spring.boot.mybatis.entity.SysUser;
import com.mba.spring.boot.mybatis.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserService implements ISysUserService {

    @Autowired
    private SysUserMapper userMapper;

    @Override
    public SysUser insert(SysUser bean) {
        bean.setId(Constants.SNOW_FLAKE.nextId());
        userMapper.insert(bean);
        return bean;
    }

    @Override
    public void delete(Long id) {
        userMapper.deleteById(id);
    }

    @Override
    public SysUser update(SysUser bean) {
        userMapper.updateById(bean);
        return bean;
    }

    @Override
    public SysUser get(Long id) {
        return userMapper.selectById(id);
    }

    @Override
    public List<SysUser> findList(SysUser bean) {
        LambdaQueryWrapper<SysUser> userQuery = Wrappers.<SysUser>lambdaQuery().likeLeft(SysUser::getName, 1);
        return userMapper.selectList(userQuery);
    }
}
